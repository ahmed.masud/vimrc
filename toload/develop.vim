" Database connections
Plug 'vim-scripts/dbext.vim'

" Testing
Plug 'janko-m/vim-test'

" NeoMake
Plug 'neomake/neomake'

" Snippets for code
Plug 'sirver/UltiSnips'
Plug 'honza/vim-snippets'

" Completion manager allow to mix various sources for completion
" and its async.
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-buffer.vim'
Plug 'prabirshrestha/asyncomplete-file.vim'
Plug 'prabirshrestha/asyncomplete-ultisnips.vim'
Plug 'prabirshrestha/asyncomplete-tags.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'

" Tags manager
Plug 'ludovicchabant/vim-gutentags'
