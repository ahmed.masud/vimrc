" Rust lang
Plug 'rust-lang/rust.vim', { 'for': 'rust' }

" Let's use racer
Plug 'racer-rust/vim-racer', { 'for': 'rust' }

" Add racer to the autocomplete for rust
Plug 'keremc/asyncomplete-racer.vim', { 'for': 'rust' }
