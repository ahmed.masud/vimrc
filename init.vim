" I'm using a dark background, so go for it
set background=dark

" Hidden buffers please
set hidden

" some basic configuration for completion
"set shortmess+=c

" Unicode
set encoding=utf-8

" Numbers
set nu

" Shorter split moves
nnoremap <C-j> <C-w><C-j>
nnoremap <C-k> <C-w><C-k>
nnoremap <C-h> <C-w><C-h>
nnoremap <C-l> <C-w><C-l>

" Default indent
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent
set fileformat=unix

" I want preview window for autocomplete and a menu even for
" just one match
set completeopt+=preview

" If we're making, lets save the file first.
set autowrite

" Let's close the location and the quickfix window if it's the last one
if exists('##QuitPre')
    autocmd QuitPre * if &filetype != 'qf' | silent! lclose | silent! cclose | endif
endif
autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif

" Open file under the cursor in a vsplit
nnoremap gf :rightbelow wincmd f<CR>

" Vim-plug {{{
call plug#begin('~/.config/nvim/plugged')

" Let's group the plugins load in a directory, will help with ansible
runtime! toload/*.vim

Plug 'kkoomen/vim-doge', { 'do': { -> doge#install() } }


call plug#end()
" End Vim-plug }}}
"
" Let's have a fancy airline bar for status
" I said fancy, it include fancy fonts
let g:airline_powerline_fonts = 1

" Leet's configure test
let test#strategy = 'neomake'

" Mapping tests
nmap <silent> t<C-n> :TestNearest<CR> " t Ctrl+n
nmap <silent> t<C-f> :TestFile<CR>    " t Ctrl+f
nmap <silent> t<C-s> :TestSuite<CR>   " t Ctrl+s
nmap <silent> t<C-l> :TestLast<CR>    " t Ctrl+l
nmap <silent> t<C-g> :TestVisit<CR>   " t Ctrl+g

" if no tabs, let's have buffer list
let g:airline#extensions#tabline#enabled = 1

" Tab navigation in the popupmenu
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<CR>"

" refresh the completion list
imap <C-space> <Plug>(asyncomplete_force_refresh)

" Remove duplicates
let g:asyncomplete_remove_duplicates = 1

let g:UltiSnipExpandTrigger="<C-e>"
" Let's register generic sources for completion
autocmd User asyncomplete_setup call asyncomplete#register_source(
    \ asyncomplete#sources#ultisnips#get_source_options({
        \ 'name': 'ultisnips',
        \ 'priority': 10,
        \ 'whitelist': ['*'],
        \ 'completor': function('asyncomplete#sources#ultisnips#completor')
    \ })
\ )

" tags are here
autocmd User asyncomplete_setup call asyncomplete#register_source(
    \ asyncomplete#sources#tags#get_source_options({
        \ 'name': 'tags',
        \ 'priority': 30,
        \ 'whitelist': ['*'],
        \ 'completor': function('asyncomplete#sources#tags#completor')
    \ })
\ )

" Let's use buffer content to complete
autocmd User asyncomplete_setup call asyncomplete#register_source(
    \ asyncomplete#sources#buffer#get_source_options({
        \ 'name': 'buffer',
        \ 'priority': 40,
        \ 'whitelist': ['*'],
        \ 'completor': function('asyncomplete#sources#buffer#completor')
    \ })
\ )

" Use filename as sources
autocmd User asyncomplete_setup call asyncomplete#register_source(
    \ asyncomplete#sources#file#get_source_options({
        \ 'name': 'file',
        \ 'priority': 40,
        \ 'whitelist': ['*'],
        \ 'completor': function('asyncomplete#sources#file#completor')
    \ })
\ )
" Remap UltiSnips action to something that's not Tab
let g:UltiSnipsExpandTrigger = "<Leader><Tab>"

" Let's use neomake to lint / make at anytime
" call neomake#configure#automake({
" \ 'TextChanged': {},
" \ 'InsertLeave': {},
" \ 'BufWritePost': {},
" \ 'BufWinEnter': {}
" \ })

" let's store the tags in a nice global places to avoid clutter in the
" projects
let g:gutentags_cache_dir = '~/.cache/gutentags'

" Toggle the tagbar on C-T
nmap <F8> :TagbarToggle<CR>

" We're using ag, might as well use it for git too
let g:gitgutter_grep = 'ag'

" Leader * starts Grepper ag
nnoremap <Leader>* :Grepper -tool ag -cword -noprompt<CR>

" Some more readable highlights for neomake
highlight NeomakeError cterm=bold ctermbg=4 ctermfg=14
highlight NeomakeCap cterm=bold ctermbg=5 ctermfg=15

" Let's have some fun with the QuickFix window
autocmd BufReadPost quickfix setlocal foldlevel=0
autocmd BufReadPost quickfix setlocal foldmethod=expr
autocmd BufReadPost quickfix setlocal foldexpr=getline(v:lnum)[0:1]=='\|\|'?1:'>1'


command! -nargs=* Hardcopy call DoMyPrint('<args>')
function DoMyPrint(args)
    let colorsave=g:colors_name
    colorscheme print
    exec 'hardcopy '.a:args
    exec 'color '.colorsave
endf
