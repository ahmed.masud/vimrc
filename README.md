Gros changement de config (du coup, perte de l'historique).

Bascule sur neovim et vim-plug pour gérer les plugins.

# Liste des plugins
  * [janko-m/vim-test](https://github.com/janko-m/vim-test) - Pour lancer les tests depuis vim (Notamment :TestNearest)
  * [vim-ariline/vim-airline](https://github.com/vim-airline/vim-airline) - Parce que ça fait une jolie barre de statut. Et que ça s'intègre de partout
  * [neomake/neomake](https://github.com/neomake/neomake) - Pour compiler, tester, linter le code en utilisant des jobs async et les quickfix
  * [majutsushi/tagbar](https://github.com/majutsushi/tagbar) - Une barre avec les tags, pour naviguer rapidement dans un projet. Pas de gestion de ctags globale
  * [sirver/UltiSnips](https://github.com/sirver/UltiSnips) et [honza/vim-snippets](https://github.com/honza/vim-snippets) - Pour gérer les snippets magique
  * [rking/ag.vim](https://github.com/rking/ag.vim) - Pour utiliser ag dans vim, au lieu de grep
  * [mhinz/vim-grepper](https://github.com/mhinz/vim-grepper) - Pour avoir un Grepper async un peu performant qui balance les résultats dans une Location
  * [airblade/vim-gitgutter](https://github.com/airblade/vim-gitgutter) - Pour afficher le status d'une ligne (+, -, modifiée) dans la marge du fichier
  * [jreybert/vimagit](https://github.com/jraybert/vimagit) - Parceque Magit est efficace et permet de gérer corrctement les commits
  * [ludovicchabant/vim-gutentags](https://github.com/ludovicchabant/vim-gutentags) - Pour gérer les ctags

## Plugins pour une autocompletion asynchrone
J'ai pas mal cherché, je voulais quelque chose de léger, sur lequel je peux ajouter plein de sources et qui, éventuellement, ne dépende pas de trop
d'autres choses. J'ai donc opté pour asyncomplete.vim, avec notamment le module lsp. L'activation des moteurs est dans [init.vim][]. Beaucoup de plugin
mais tout est documenté sur la page du projet.
  * [prabirshrestha/async.vim](https://github.com/prabirshrestha/async.vim)
  * [prabirshrestha/vim-lsp](https://github.com/prabirshrestha/vim-lsp)
  * [prabirshrestha/asyncomplete.vim](https://github.com/prabirshrestha/asyncomplete.vim)
  * [prabirshrestha/asyncomplete-buffer.vim](https://github.com/prabirshrestha/asyncomplete-buffer.vim)
  * [prabirshrestha/asyncomplete-file.vim](https://github.com/prabirshrestha/asyncomplete-file.vim)
  * [keremc/asyncomplete-racer.vim](https://github.com/keremc/asyncomplete-racer.vim)
  * [prabirshrestha/asyncomplete-ultisnips.vim](https://github.com/prabirshrestha/asyncomplete-ultisnips.vim)
  * [prabirshrestha/asyncomplete-tags.vim](https://github.com/prabirshrestha/asyncomplete-tags.vim)
  * [prabirshrestha/asyncomplete-lsp.vim](https://github.com/prabirshrestha/asyncomplete-lsp.vim)

## Plugins spécifiques à Rust
J'écris du rust, du coup, quelques plugins spécifiques. La config spécifique est dans [ftplugin/rust.vim][]
  * [rust-lang/rust.vim](https://github.com/rust-lang/rust.vim) - Plein de choses pour rust (syntaxe, snippets, etc)
  * [racer-rust/vim-racer](https://github.com/racer-rust/vim.racer) - Pour utiliser racer dans vim (surtout utilie pour la completion auto)
